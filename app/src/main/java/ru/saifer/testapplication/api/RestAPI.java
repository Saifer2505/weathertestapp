package ru.saifer.testapplication.api;

import retrofit2.http.GET;
import retrofit2.http.Query;
import ru.saifer.testapplication.api.models.WeatherDailyData;
import rx.Observable;

/**
 * Created by Saifer on 16.01.2017.
 */

public interface RestAPI {

    String API_BASE_URL = "http://api.openweathermap.org/data/2.5/";

    @GET("forecast/daily?")
    Observable<WeatherDailyData> getWeatherDailyData(
            @Query("q") String city,
            @Query("mode") String mode,
            @Query("units") String units,
            @Query("cnt") int count,
            @Query("appid") String appid
    );
}
