package ru.saifer.testapplication.ui.fragment;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import ru.saifer.testapplication.R;
import ru.saifer.testapplication.adapter.WeatherAdapter;
import ru.saifer.testapplication.api.models.WeatherDailyData;
import ru.saifer.testapplication.presenter.impl.WeatherPresenterImpl;
import ru.saifer.testapplication.view.WeatherView;

/**
 * Created by Saifer on 15.01.2017.
 */

public class WeatherFragment extends Fragment implements WeatherView, SwipeRefreshLayout.OnRefreshListener, View.OnKeyListener {

    private EditText cityName;
    private TextView errorLabel;
    private SwipeRefreshLayout refreshLayout;
    private RecyclerView weatherList;

    private WeatherPresenterImpl presenter;
    private WeatherAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.weather, container, false);

        cityName = (EditText) view.findViewById(R.id.cityName);
        cityName.setOnKeyListener(this);

        errorLabel = (TextView)view.findViewById(R.id.errorLabel);

        refreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.refreshLayout);
        refreshLayout.setOnRefreshListener(this);

        adapter = new WeatherAdapter();
        weatherList = (RecyclerView) view.findViewById(R.id.weatherList);
        weatherList.setLayoutManager(new LinearLayoutManager(getActivity()));
        weatherList.setAdapter(adapter);

        presenter = new WeatherPresenterImpl(this);

        return view;
    }

    private void showWeatherByCity(String city) {
        errorLabel.setVisibility(View.INVISIBLE);
        presenter.requestWeatherDailyData(city);
        hideKeyboard(getActivity(), cityName.getApplicationWindowToken());
    }

    @Override
    public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {
        if ((keyEvent.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
            showWeatherByCity(cityName.getText().toString().trim());
            return true;
        }
        return false;
    }

    @Override
    public void onRefresh() {
        showWeatherByCity(cityName.getText().toString().trim());
    }

    @Override
    public void showProgress() {
        adapter.clear();
        if (!refreshLayout.isRefreshing()) {
            refreshLayout.post(() -> refreshLayout.setRefreshing(true));
        }
    }

    @Override
    public void hideProgress() {
        refreshLayout.post(() -> refreshLayout.setRefreshing(false));
    }

    @Override
    public void onErrorMessage(String msg) {
        adapter.clear();
        errorLabel.setText(msg);
        errorLabel.setVisibility(View.VISIBLE);
    }

    @Override
    public void onWeatherDailyData(WeatherDailyData data) {
        if (data != null && data.getList() != null) {
            adapter.setData(data.getList());
        }
    }

    public static boolean hideKeyboard(Context context, IBinder binder) {
        try {
            InputMethodManager imm = (InputMethodManager)context.getSystemService(Context.INPUT_METHOD_SERVICE);
            return imm.hideSoftInputFromWindow(binder, 0);
        } catch(Exception e) {
            e.printStackTrace();
        }
        return false;
    }

}
