package ru.saifer.testapplication.ui.activity;

import android.app.Fragment;
import android.os.Bundle;

import ru.saifer.testapplication.R;
import ru.saifer.testapplication.baseview.BaseActivity;
import ru.saifer.testapplication.ui.fragment.WeatherFragment;

public class MainActivity extends BaseActivity {

    //private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //initViews();
        //setSupportActionBar(toolbar);

        Fragment fragment = getFragmentManager().findFragmentByTag(WeatherFragment.class.getSimpleName());
        if (fragment == null) {
            fragment = new WeatherFragment();
            getFragmentManager()
                    .beginTransaction()
                    //.add(fragment, WeatherFragment.class.getSimpleName())
                    .replace(R.id.container, fragment, WeatherFragment.class.getSimpleName())
                    .commit();
        }

    }

    /*private void initViews() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
    }*/

}
