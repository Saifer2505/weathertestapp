package ru.saifer.testapplication.view;

import ru.saifer.testapplication.api.models.WeatherDailyData;

/**
 * Created by Saifer on 15.01.2017.
 */

public interface WeatherView {

    void showProgress();
    void hideProgress();
    void onErrorMessage(String msg);
    void onWeatherDailyData(WeatherDailyData data);
}
