package ru.saifer.testapplication.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import ru.saifer.testapplication.R;
import ru.saifer.testapplication.api.models.List;
import ru.saifer.testapplication.api.models.Weather;

/**
 * Created by Saifer on 16.01.2017.
 */

public class WeatherAdapter extends RecyclerView.Adapter<WeatherAdapter.ViewHolder> {

    private java.util.List<List> mDataset;
    private SimpleDateFormat mDateFormat;
    private Date mDate;

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView weekName;
        public TextView dateName;
        public TextView humidity;
        public TextView maxTemp;
        public TextView minTemp;
        public TextView weatherDescr;

        public ViewHolder(View v) {
            super(v);
            weekName = (TextView) v.findViewById(R.id.weekName);
            dateName = (TextView) v.findViewById(R.id.dateName);
            maxTemp = (TextView) v.findViewById(R.id.maxTemp);
            minTemp = (TextView) v.findViewById(R.id.minTemp);
            weatherDescr = (TextView) v.findViewById(R.id.weatherDescr);
            humidity = (TextView) v.findViewById(R.id.humidity);
        }
    }

    public WeatherAdapter() {
        mDateFormat = new SimpleDateFormat("dd.MM", Locale.getDefault());
        mDate = new Date();
    }

    public void setData(java.util.List<List> dataset) {
        mDataset = dataset;
        notifyDataSetChanged();
    }

    public void clear() {
        mDataset = null;
        notifyDataSetChanged();
    }

    @Override
    public WeatherAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.weather_list_item, parent, false);

        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        List item = mDataset.get(position);

        // День недели (Например: "Пн")
        holder.weekName.setText(parseDate((long)item.getDt() * 1000, "EE"));
        // Дата (Например: "16.01")
        holder.dateName.setText(parseDate((long)item.getDt() * 1000, "dd.MM"));

        // Максимальная температура
        holder.maxTemp.setText(String.format(Locale.getDefault(), "%.0f\u00B0", item.getTemp().getMax()));
        // Минимальная температура
        holder.minTemp.setText(String.format(Locale.getDefault(), "%.0f\u00B0", item.getTemp().getMin()));

        java.util.List<Weather> weatherList = item.getWeather();
        if (weatherList != null && !weatherList.isEmpty()) {
            holder.weatherDescr.setVisibility(View.VISIBLE);
            // Короткое описание погоды
            holder.weatherDescr.setText(weatherList.get(0).getDescription());
        } else {
            holder.weatherDescr.setVisibility(View.INVISIBLE);
        }

        // Влажность воздуха
        holder.humidity.setText(String.format(Locale.getDefault(), "%d%%", item.getHumidity()));
    }

    @Override
    public int getItemCount() {
        return mDataset == null ? 0 : mDataset.size();
    }

    public String parseDate(long date, String pattern) {
        mDate.setTime(date);
        mDateFormat.applyPattern(pattern);
        return mDateFormat.format(mDate);
    }

}
