package ru.saifer.testapplication.presenter;

/**
 * Created by Saifer on 15.01.2017.
 */

public interface WeatherPresenter {

    void requestWeatherDailyData(String cityName);
}
