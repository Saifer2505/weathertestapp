package ru.saifer.testapplication.presenter.impl;

import ru.saifer.testapplication.api.WeatherService;
import ru.saifer.testapplication.presenter.WeatherPresenter;
import ru.saifer.testapplication.view.WeatherView;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Saifer on 15.01.2017.
 */

public class WeatherPresenterImpl implements WeatherPresenter {

    private WeatherView view;

    public WeatherPresenterImpl(WeatherView view) {
        this.view = view;
    }

    @Override
    public void requestWeatherDailyData(String cityName) {
        view.showProgress();

        WeatherService.getInstance().getWeatherDailyData(cityName, "json", "metric", 7, "3093e50731a12ed57d9b290e70bf464d")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(weatherData -> {
                    view.onWeatherDailyData(weatherData);
                    view.hideProgress();
                }, throwable -> {//Throwable::printStackTrace);
                    throwable.printStackTrace();
                    view.onErrorMessage(throwable.getMessage());
                    view.hideProgress();
                });

    }
}
